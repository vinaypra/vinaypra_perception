#!/usr/bin/env python

# Import necessary dependencies
import rospy
from cmvision.msg import Blobs, Blob
from geometry_msgs.msg import Twist

head_rot = 0.0
blob_pos_x = 0.0
blob_pos_y = 0.0

def blob_cb(data):
    # Filter blob here
    global head_rot
    global blob_pos_x
    global blob_pos_y

    for i in data.blobs:

        # Check if detected blob is RedBall
        if i.name == "RedBall":

            # Store the x and y locations of the RedBall
            blob_pos_x = i.x
            blob_pos_y = i.y

            # Verify on console
            rospy.loginfo("x-position: %s"%blob_pos_x)
            rospy.loginfo("y-position: %s"%blob_pos_y)

            # If head is centered then we need not turn it
            if(blob_pos_x < 180 and blob_pos_x > 220):
                head_rot = 0.0 # No turning

            # Adjust head to left, if RedBall is to the right
            if(blob_pos_x < 180):

                    # left -> +ve ; right -> -ve
                    head_rot = 0.25
            
            # Adjust head to right, if RedBall is to the left
            if(blob_pos_x > 220):

                    # left -> +ve ; right -> -ve
                    head_rot = -0.25
        
        else:
            # No action for any other blob
            head_rot = 0.0

def main():

    # Initialize node
    rospy.init_node("u_12_script_node")

    global head_rot
    global blob_pos_x
    global blob_pos_y

    # Define subscriber for blob message
    blob_sub = rospy.Subscriber('/blobs', Blobs, blob_cb)

    # Define publisher for twist message into the topic 
    # named /mira/commands/velocity to turn Mira's head
    mira_pub = rospy.Publisher('/mira/commands/velocity', Twist, queue_size=1)

    # Create an instance of Twist
    mira_pub_obj = Twist()

    while not rospy.is_shutdown():
        # If head_rot is zero, then we don't have to turn Mira's head
        if (head_rot == 0.0):
            # Hence, set everything along with angular.z to 0.0
            mira_pub_obj.linear.x = 0.0
            mira_pub_obj.linear.y = 0.0
            mira_pub_obj.linear.z = 0.0
            mira_pub_obj.angular.x = 0.0
            mira_pub_obj.angular.y = 0.0
            mira_pub_obj.angular.z = 0.0

        # If head_rot is not zero, then we have to turn Mira's head
        elif (head_rot != 0.0):
            # Hence, set everything to 0.0 except angular.z (=head.rot)
            mira_pub_obj.linear.x = 0.0
            mira_pub_obj.linear.y = 0.0
            mira_pub_obj.linear.z = 0.0
            mira_pub_obj.angular.x = 0.0
            mira_pub_obj.angular.y = 0.0
            mira_pub_obj.angular.z = head_rot

        # Publish the vtwist values
        mira_pub.publish(mira_pub_obj)
        rospy.sleep(1)


if __name__ == '__main__':
    try:
        main()
        
    except rospy.ROSInterruptException: 
        pass