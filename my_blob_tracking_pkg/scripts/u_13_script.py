#!/usr/bin/env python

# Import necessary dependencies
import rospy
from cmvision.msg import Blobs, Blob
from geometry_msgs.msg import Twist
from sensor_msgs.msg import JointState
from std_msgs.msg import Float64

#mira_joints_dict = None
#mira_joints_info = None
#mira_yaw_update = 0.0

class MiraHeadMove(object):

    def __init__(self):

        #global mira_joints_dict
        #global mira_joints_info
        #global mira_yaw_update
        self.mira_yaw_update = 0.0

        # Define subscriber for reading Joint State values for roll, pitch and yaw values
        self.mira_sub_joints = rospy.Subscriber('/mira/joint_states', JointState, self.mira_joints_cb)

        mira_joints_info = None
        #rospy.sleep(5)
        while mira_joints_info is None:
            try:
                mira_joints_info = rospy.wait_for_message('/mira/joint_states', JointState, timeout=5)
            except:
                pass


        # Get the values for joint name and current position of the joint
        self.mira_joints_dict = dict(zip(mira_joints_info.name, mira_joints_info.position))

        # Define subscriber for reading angular.z value
        self.mira_sub_vel = rospy.Subscriber('/mira/commands/velocity', Twist, self.blob_cb)

        #Now we move all the joints with the corressponding roll, pitch yaw values
        # We only need to feed in yaw value to rotate the head
        # Hence roll and pitch will be set zero

        # Define publisher for roll
        self.mira_pub_roll = rospy.Publisher('/mira/roll_joint_position_controller/command', Float64, queue_size=1)
        # Create an instance of Float64
        mira_pub_roll_obj = Float64()
        mira_pub_roll_obj.data = 0.0
        # Publish the roll value for joint
        self.mira_pub_roll.publish(mira_pub_roll_obj)

        # Define publisher for pitch
        self.mira_pub_pitch = rospy.Publisher('/mira/pitch_joint_position_controller/command', Float64, queue_size=1)
        # Create an instance of Float64
        mira_pub_pitch_obj = Float64()
        mira_pub_pitch_obj.data = 0.0
        # Publish the pitch value for joint
        self.mira_pub_pitch.publish(mira_pub_pitch_obj)

        # Define publisher for yaw
        self.mira_pub_yaw = rospy.Publisher('/mira/yaw_joint_position_controller/command', Float64, queue_size=1)
        # Create an instance of Float64
        mira_pub_yaw_obj = Float64()
        mira_pub_yaw_obj.data = self.mira_yaw_update
        # Publish the yaw value for joint
        self.mira_pub_yaw.publish(mira_pub_yaw_obj)

        #rospy.loginfo("Yaw value" + str(self.mira_yaw_update))


    def mira_joints_cb(self, msg):

        #global mira_joints_dict
        #global mira_joints_info
        #global mira_yaw_update

        self.mira_joints_dict = dict(zip(msg.name, msg.position))


    def blob_cb(self, msg):

        #global mira_joints_dict
        #global mira_joints_info
        #global mira_yaw_update

        mira_ang_z = msg.angular.z
        #rospy.loginfo("Angular value" + str(mira_ang_z ))
        mira_yaw_current = self.mira_joints_dict.get("yaw_joint")
        self.mira_yaw_update = mira_ang_z + mira_yaw_current
        #rospy.loginfo("Yaw value" + str(self.mira_yaw_update))


if __name__ == '__main__':
    try:
        # Initialize node
        rospy.init_node('u_13_script_node')

        # Call the class
        mira_move_obj = MiraHeadMove()
        rospy.spin()

    except rospy.ROSInterruptException:
        pass
